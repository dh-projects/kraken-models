# Models

Un dossier pour centraliser les modèles entraînés à l'aide d'eScriptorium/Kraken.

## A propos des modèles

-> les modèles sont de préférence associés à une [issue](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues) correspondant à la documentation des expérimentations ayant mené à la production du modèle.

-> ils doivent être associés à une ligne dans le tableau suivant résumant brièvement les données qu'ils sont susceptibles de prendre en charge.

## Détail des modèles de **transcription** :

| modèle | description | related issue |
| :----- | :---------- | :-----------: |
| model\_marotte\_bin\_accuracy-8164.mlmodel | entraîné sur les GT du dossier "Etude Marotte" (ID: 42147) soit 67 pages pour 8904 objets d'entaînement. Entraînement avec paramétrage par défaut de Kraken (`ketos train training_bits/*.png`) |  |
| CM_ftGL26_13.mlmodel | entraîné en affinant generic_lectaurep_26 pour les contrats de mariage (LECTAUREP) | [8](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/8) |
| bronod_fs_66.mlmodel | entraîné from scratch pour l'écriture du notaire Bronod (LECTAUREP) | [14](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/14) |
| bronod_ftgen26_12.mlmodel | entraîné en affinant generic_lectaurep_26 pour l'écriture du notaire Bronod (LECTAUREP) | [14](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/14) |
| generic_lectaurep_26.mlmodel | modèle mixte - entraîné from scratch à partir d'une variété d'écritures administratives du XIXe siècle (LECTAUREP) | [8](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/8) |
| mixte_mrs_15.mlmodel | modèle mixte - entraîné from scratch à partir d'une varété d'écritures administratives du XIXe siècle (LECTAUREP) | [17](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/17) |
| riant_frmrs15_12.mlmodel | entraîné en affinant (50 pages) mixte_mrs_15 pour l'écriture du notaire Riant (LECTAUREP) | [18](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/18) |
| cm_ft_gen26_5.mlmodel | entraîné en affinant (44 pages) generic_lectaurep_26 sur les contrats de mariage (LECTAUREP) | [20](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/20) | 
| cm_ft_mrs15_11.mlmodel | entraîné en affinant (44 pages) mixte_mrs_15 sur les contrats de mariage (LECTAUREP) | [20](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/20) |
| bronod2_ft_gen_10.mlmodel | entraîné en affinant (125 pages) generic_lectaurep_26 pour l'écriture du notaire Bronod (LECTAUREP) | [14](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/14) |
| bronod2_ft_mrs_16.mlmodel | entraîné en affinant (125 pages) mixte_mrs_15  pour l'écriture du notaire Bronod (LECTAUREP) | [14](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/14) |
| bronod_ft2_7.mlmodel | entraîné en affinant (25 pages) bronod_ftgen26_12 pour l'écriture du notaire Bronod (LECTAUREP) | [14](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/14) |

## Détail des modèles de **segmentation** :


| modèle | description | related issue |
| :----- | :---------- | :-----------: |
| model\_marotte\_bin\_accuracy-8164.mlmodel | entraîné sur les GT du dossier "Etude Marotte" (ID: 42147) soit 67 pages pour 8904 objets d'entaînement. Entraînement avec paramétrage par défaut de Kraken (`ketos train training_bits/*.png`) |
| blla\_ft\_lectaurep\_logical_structure.mlmodel | entraîné en affinant un modèle déjà affiné de blla ; Vise à segmenter la structure logique des pages des répertoires des notaires ainsi ques les premières lignes de chaque enregistrement | [issue 22](https://gitlab.inria.fr/dh-projects/kraken-models/-/issues/22) |
| | |
